{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "from abc import ABC, abstractmethod\n",
    "from concurrent.futures import ProcessPoolExecutor, ThreadPoolExecutor\n",
    "from dataclasses import dataclass\n",
    "from enum import IntEnum, auto\n",
    "from multiprocessing import Process\n",
    "from typing import Callable\n",
    "\n",
    "import numpy as np\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Parallel programming in Python\n",
    "\n",
    "- About executing multiple independent tasks simultaneously\n",
    "- Not to be confused with concurrency, which is composing independent processes to work together\n",
    "- Parallel computing **can** speed up portions of your code **to a degree**\n",
    "- However, setting up parallel computing requires some computational effort as well\n",
    "- Maximal speedup depends on the task at hand, see [Amdahl's law](https://en.wikipedia.org/wiki/Amdahl%27s_law)\n",
    "\n",
    "Examples for both\n",
    "\n",
    "- Processes\n",
    "- ~~OS / Green~~ Threads\n",
    "- ~~Protothreads~~\n",
    "- ~~Fibers~~\n",
    "- ~~Generators~~\n",
    "- ~~Co-routines~~\n",
    "- ~~Actors~~\n",
    "\n",
    "We will only talk about processes and threads, but to understand how they differ, we need a little background.  "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Memory layout\n",
    "\n",
    "When you start your Python interpreter (or any other program for that matter), a new **process** is created for it.  \n",
    "Your operating system allocates some memory for that process, however not all memory is created equally.  \n",
    "Here we will only talk about the stack and the heap.  \n",
    "\n",
    "<img src=\"stack-vs-heap.webp\" style=\"max-width: 500px\">\n",
    "\n",
    "- Stack\n",
    "  - Ordered, very fast, but limited size\n",
    "  - Works as last in, first out (LIFO)\n",
    "  - Small things like integers are located hear\n",
    "- Heap\n",
    "  - Unordered collection of large objects (arrays etc)\n",
    "  - Slower, but immense size\n",
    "\n",
    "**Quick quiz**  \n",
    "\n",
    "When a function is called, the return address is pushed onto the stack, so that it can be returned to after the function has finished.  \n",
    "There is a catch here for recursive functions (functions calling themselves) though.  \n",
    "What happens, if recursive functions are nested to deeply? \n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Checking if parallelisation makes sense\n",
    "\n",
    "- How much of the task can be parallelised?\n",
    "- Does task require a lot of communication?\n",
    "- Does task use library functions that are already parallel?\n",
    "- Do extra performance requirements of parallelising offset the cost of the task? \n",
    "- If in doubt - measure!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Bad choice, library function is already parallel\n",
    "# Check htop while running this\n",
    "_ = np.linalg.inv(np.random.random((10000, 10000)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ways of parellelising work\n",
    "\n",
    "### Processes\n",
    "\n",
    "- Each process has it's own stack and heap, so processes cannot directly access each other's memory\n",
    "  - This *can* be a security benefit!\n",
    "- Expensive to create\n",
    "  - A new process needs to be created (forked on Unix, spawn on OSX and Windows)\n",
    "  - Forking essentially copies the entire process\n",
    "  - Spawning creates a new process an re-imports the current module, creating new versions of all variables\n",
    "\n",
    "\n",
    "### Threads\n",
    "\n",
    "- Separate line of execution within a process\n",
    "- Each thread has it's own stack, but heap is shared\n",
    "- Cheap to create\n",
    "- In Python (currently) blocked one thread is active at a time though, due to GIL\n",
    "- So threads give no performance boost for the tasks we are usually working on\n",
    "- Still make sense if you are waiting on I/O a lot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Multiprocessing in Python\n",
    "\n",
    "Let's start with the manual approach.  \n",
    "You can create a Process with the `multiprocessing.Process` class.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def target():\n",
    "    print('process id:', os.getpid())\n",
    "\n",
    "p = Process(target=target)\n",
    "p.start()  # Start child process\n",
    "p.join()  # Wait until child process terminates"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A single extra process doesn't really help us, so we can create multiple.  \n",
    "**Quick quiz**: Does the code below run in parallel?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ps = [Process(target=target) for _ in range(5)]\n",
    "\n",
    "for p in ps:\n",
    "    p.start()\n",
    "    p.join()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you let run code in parallel, watch out for shared mutable state!  \n",
    "E.g. writing to files from multiple threads / processes is usually a bad idea.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ps = [Process(target=target) for _ in range(5)]\n",
    "\n",
    "for p in ps:\n",
    "    p.start()\n",
    "for p in ps:\n",
    "    p.join()\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Our manual approach has multiple drawbacks.  \n",
    "\n",
    "- It's annoying\n",
    "- We have to keep track of how many CPU cores we actually have\n",
    "- Actually returning data requires a `multiprocessing.Manager` object\n",
    "- Fun stuff starts to happen if we don't `join` processes etc.\n",
    "- If you have `len(data) > n_cores`, you might want to keep processes open and just feed them new data in order to avoid the cost of setting up the process\n",
    "- `multiprocessing.Pool` solves most of this, but we will use the newer `concurrent.futures` from now on, because the APIs are nicer.\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Concurrent futures\n",
    "\n",
    "Manually starting and joining processes is annoying and error prone.  \n",
    "You want to be able to loop over your data in a more straightforward way.  \n",
    "The currently advised way to do that is using the `ProcessPoolExecutor` from the `concurrent.futures` standard library.  \n",
    "The `ProcessPoolExecutor` supplies a `map` function, which essentially is a `for-loop` and guarantees that the output order matches the input order.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def target_2(x: int) -> int:\n",
    "    return x\n",
    "\n",
    "# Normal map function takes a function and an iterable as input\n",
    "print(list(map(target_2, range(12))))\n",
    "\n",
    "# Parallel map function just as well\n",
    "with ProcessPoolExecutor() as pe:\n",
    "    print(list(pe.map(target_2, range(12))))\n",
    "\n",
    "# If calculation time per piece of data is low, it makes sense to separate your data into\n",
    "# \"chunks\", which are then passed to each process\n",
    "with ProcessPoolExecutor() as pe:\n",
    "    print(list(pe.map(target_2, range(12), chunksize=12)))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Speed comparison"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fibonacci(n: int) -> int:\n",
    "    if n <= 1:\n",
    "        return n\n",
    "    return fibonacci(n - 2) + fibonacci(n - 1)\n",
    "\n",
    "def do_work_then_return_arg(x: int):\n",
    "    return fibonacci(35) + x\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(list(map(do_work_then_return_arg, range(12))))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with ThreadPoolExecutor() as te:\n",
    "    te_res = list(te.map(do_work_then_return_arg, range(12)))\n",
    "\n",
    "print(te_res)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with ProcessPoolExecutor() as pe:\n",
    "    pe_res = list(pe.map(do_work_then_return_arg, range(12)))\n",
    "\n",
    "print(pe_res)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sharp edges\n",
    "\n",
    "- Numpy random number generation\n",
    "- Windows only allows spawning new processes in a main file (doesn't work with jupyter)\n",
    "- Spawning requires everything to be `pickle`d, which sadly lambda functions can't\n",
    "\n",
    "\n",
    "Sources\n",
    "- https://numpy.org/doc/stable/reference/random/parallel.html\n",
    "- https://www.pythonforthelab.com/blog/differences-between-multiprocessing-windows-and-linux/\n",
    "- https://medium.com/@grvsinghal/speed-up-your-python-code-using-multiprocessing-on-windows-and-jupyter-or-ipython-2714b49d6fac"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# import pickle\n",
    "# import tempfile\n",
    "\n",
    "# # This one works fine\n",
    "# with tempfile.TemporaryFile() as fp:\n",
    "#     pickle.dump({}, fp)\n",
    "\n",
    "# # This one crashes\n",
    "# with tempfile.TemporaryFile() as fp:\n",
    "#     pickle.dump(lambda x: x, fp)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def bad_numpy(_: int) -> float:\n",
    "    return np.random.random()\n",
    "\n",
    "with ProcessPoolExecutor() as pe:\n",
    "    print(list(pe.map(bad_numpy, range(5))))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def better_numpy(_: int) -> float:\n",
    "    rng = np.random.default_rng()\n",
    "    return rng.random()\n",
    "\n",
    "with ProcessPoolExecutor() as pe:\n",
    "    print(list(pe.map(better_numpy, range(5))))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def best_numpy(x: int) -> float:\n",
    "    rng = np.random.default_rng(seed=x)\n",
    "    return rng.random()\n",
    "\n",
    "with ProcessPoolExecutor() as pe:\n",
    "    print(list(pe.map(best_numpy, range(5))))\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Pair programming\n",
    "\n",
    "- Point of pair programming is to avoid having to context-switch between different levels of abstraction\n",
    "- e.g. lower-level coding vs higher-level biological interpretation\n"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Type checkers\n",
    "\n",
    "- Essentially automatic pair programming\n",
    "- Always there, doesn't cost anything"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(x: int) -> int:\n",
    "    ...\n",
    "\n",
    "\n",
    "def g(x: float) -> float:\n",
    "    ...\n",
    "\n",
    "\n",
    "def requires_int(x: int, fn: Callable[[int], int]) -> int:\n",
    "    return fn(x)\n",
    "\n",
    "requires_int(1, f)\n",
    "requires_int(1, g)  # g flagged as type error!"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hands-on: Pratt parsing\n",
    "\n",
    "This task is purposefully hard to mimic real-world problems.  \n",
    "Your task is to write a parser for a very simple calculator.  \n",
    "The calculator can only take in single digits and the operands `+, -, *, /`.  \n",
    "However, it *does correctly take precedence and associativity into account*.  \n",
    "For this, we are using a technique called Pratt Parsing, first described by Vaughan Pratt in the 1973 paper \"Top down operator precedence\".  \n",
    "There is [this very nice tutorial on Pratt parsing](https://matklad.github.io/2020/04/13/simple-but-powerful-pratt-parsing.html), but it's written in the Rust programming language instead of Python.  \n",
    "As usual in parsing, we don't work directly on the source material, but first use a `lexer` to transform the source into `tokens`, which carry additional information.  \n",
    "The lexer is already provided in the code below.  \n",
    "\n",
    "For this exercise, partner up as teams of two.  \n",
    "One person is the designated coder, the other person should keep track of the bigger-picture logic.  \n",
    "A good start might be for one person to understand the Rust code, while the other person makes themselves accustomed to the lexer and data layout provided.  \n",
    "At the end of the task **both** people should be capable of explaining the resulting code (and might actually have to 😀).  \n",
    "\n",
    "\n",
    "### Some notes\n",
    "\n",
    "- The tutorial uses binding power instead of precedence and associativity, which is explained in the article.  \n",
    "- `+` and `-` will have left binding power 1 and right binding power 2.  \n",
    "- `*` and `/` will have left binding power 3 and right binding power 4.  \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This are the types used for the lexer (tokens)\n",
    "# All tokens are modelled as just a single class, but contain\n",
    "# an enum differentiating the different variants\n",
    "# Would it have made sense to use subtyping here?\n",
    "\n",
    "\n",
    "class TokenType(IntEnum):\n",
    "    Number = auto()\n",
    "    Operator = auto()\n",
    "\n",
    "\n",
    "@dataclass\n",
    "class Token:\n",
    "    type: TokenType\n",
    "    value: str\n",
    "\n",
    "\n",
    "# These are the types for the parser (AST = abstract syntax tree)\n",
    "# All nodes of the AST are modelled as subclasses of a base AstNode,\n",
    "# which contains an abstract method required for all subclasses to implement\n",
    "# Would it have made sense to use the Enum approach used for the tokens?\n",
    "\n",
    "class AstNode(ABC):\n",
    "    @abstractmethod\n",
    "    def to_sexpr(self) -> str:\n",
    "        ...\n",
    "\n",
    "\n",
    "@dataclass\n",
    "class IntLiteral(AstNode):\n",
    "    value: str\n",
    "\n",
    "    def to_sexpr(self) -> str:\n",
    "        return self.value\n",
    "\n",
    "\n",
    "@dataclass\n",
    "class BinOp(AstNode):\n",
    "    op: str\n",
    "    ops: tuple[AstNode, AstNode]\n",
    "\n",
    "    def to_sexpr(self) -> str:\n",
    "        op1, op2 = self.ops\n",
    "        return f\"({self.op} {op1.to_sexpr()} {op2.to_sexpr()})\"\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This is the actual lexer implementation\n",
    "# For simplicity this is just a simple function that \n",
    "# returns a list of all the tokens\n",
    "\n",
    "def lex(s: str) -> list[Token]:\n",
    "    tokens = []\n",
    "    for char in s:\n",
    "        if char.isspace():\n",
    "            continue\n",
    "\n",
    "        if char.isnumeric():\n",
    "            tokens.append(Token(type=TokenType.Number, value=char))\n",
    "        elif char in {\"+\", \"-\", \"*\", \"/\"}:\n",
    "            tokens.append(Token(type=TokenType.Operator, value=char))\n",
    "        else:\n",
    "            raise ValueError(f\"Unknown input {char}\")\n",
    "    return tokens\n",
    "\n",
    "\n",
    "assert lex(\"\") == []\n",
    "assert lex(\"1 + 2\") == [\n",
    "    Token(TokenType.Number, \"1\"),\n",
    "    Token(TokenType.Operator, \"+\"),\n",
    "    Token(TokenType.Number, \"2\"),\n",
    "]\n",
    "assert lex(\"1 - 2\") == [\n",
    "    Token(TokenType.Number, \"1\"),\n",
    "    Token(TokenType.Operator, \"-\"),\n",
    "    Token(TokenType.Number, \"2\"),\n",
    "]\n",
    "assert lex(\"1 * 2\") == [\n",
    "    Token(TokenType.Number, \"1\"),\n",
    "    Token(TokenType.Operator, \"*\"),\n",
    "    Token(TokenType.Number, \"2\"),\n",
    "]\n",
    "assert lex(\"1 / 2\") == [\n",
    "    Token(TokenType.Number, \"1\"),\n",
    "    Token(TokenType.Operator, \"/\"),\n",
    "    Token(TokenType.Number, \"2\"),\n",
    "]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def infix_binding_power(op: Token) -> tuple[int, int]:\n",
    "    match op.value:\n",
    "        case \"+\" | \"-\":\n",
    "            return 1, 2\n",
    "        case \"*\" | \"/\":\n",
    "            return 3, 4\n",
    "        case _:\n",
    "            raise ValueError(f\"Unknown operator {op}\")\n",
    "\n",
    "\n",
    "assert infix_binding_power(Token(TokenType.Number, \"+\")) == (1, 2)\n",
    "assert infix_binding_power(Token(TokenType.Number, \"-\")) == (1, 2)\n",
    "assert infix_binding_power(Token(TokenType.Number, \"*\")) == (3, 4)\n",
    "assert infix_binding_power(Token(TokenType.Number, \"/\")) == (3, 4)\n",
    "\n",
    "\n",
    "def expr(tokens: list[Token], min_bp: int) -> AstNode:\n",
    "    if len(tokens) == 0:\n",
    "        raise ValueError(\"Incomplete input\")\n",
    "\n",
    "    if (tok := tokens.pop()).type != TokenType.Number:\n",
    "        raise ValueError(f\"Bad number: {tok}\")\n",
    "    lhs = IntLiteral(tok.value)\n",
    "\n",
    "    while len(tokens) > 0:\n",
    "        op = tokens[-1]\n",
    "        if op.type != TokenType.Operator:\n",
    "            raise ValueError(f\"Bad operator: {op}\")\n",
    "\n",
    "        l_bp, r_bp = infix_binding_power(op)\n",
    "        if l_bp < min_bp:\n",
    "            break\n",
    "\n",
    "        _ = tokens.pop()\n",
    "\n",
    "        rhs = expr(tokens, r_bp)\n",
    "        lhs = BinOp(op.value, (lhs, rhs))\n",
    "    return lhs\n",
    "\n",
    "\n",
    "def parse(tokens: list[Token]) -> AstNode:\n",
    "    return expr(tokens=list(reversed(tokens)), min_bp=0)\n",
    "\n",
    "\n",
    "assert parse(lex(\"1\")).to_sexpr() == \"1\"\n",
    "assert parse(lex(\"1 + 2\")).to_sexpr() == \"(+ 1 2)\"\n",
    "assert parse(lex(\"1 + 2 * 3\")).to_sexpr() == \"(+ 1 (* 2 3))\"\n",
    "assert parse(lex(\"1 + 2 * 3 + 4\")).to_sexpr() == \"(+ (+ 1 (* 2 3)) 4)\"\n",
    "assert parse(lex(\"1 + 2 * 3 * 4 + 5\")).to_sexpr() == \"(+ (+ 1 (* (* 2 3) 4)) 5)\"\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Code review\n",
    "\n",
    "- We all make small mistakes \n",
    "  - Having your work checked by someone else helps to reduce this\n",
    "  - For fun, search for \"Reinhart and Rogoff Excel blunder\" and think about how this affected European politics\n",
    "- We all write unmaintainable stuff from time to time\n",
    "  - Having to explain your work helps to reduce this\n",
    "\n",
    "The issue here is that people don't have loads of time, so we should aim at making the process as easy as possible."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tools to help\n",
    "\n",
    "- `git diff` shows changes between two commits\n",
    "- It's pretty clear what changes between two versions below\n",
    "\n",
    "![image.png](git-diff.png)\n",
    "\n",
    "- Can someone tell me what changes in the example below?\n",
    "\n",
    "<img src=\"git-diff-bad.png\" style=\"max-width: 500px\">\n",
    "\n",
    "\n",
    "\n",
    "In order for `git diff` to be as concise as possible, it's a good idea to use linters and formatters\n",
    "\n",
    "- [black](https://pypi.org/project/black/) is an awesome auto-formatter\n",
    "- [isort](https://pypi.org/project/isort/) sorts your imports\n",
    "\n",
    "If you are using jupyter notebooks, it's a good idea to `Clear All Outputs` before you commit, in order for images and other noise not to appear in `git diff`.\n",
    "\n",
    "A very good idea here is to add a [pre-commit hook](https://pre-commit.com/) to automatically run scripts before `git commit` is run. Here is an example configuration to automatically clear the notebook state:\n",
    "\n",
    "```\n",
    "repos:\n",
    "  - repo: local\n",
    "    hooks:\n",
    "      - id: jupyter-nb-clear-output\n",
    "        name: jupyter-nb-clear-output\n",
    "        files: \\.ipynb$\n",
    "        stages: [commit]\n",
    "        language: system\n",
    "        entry: jupyter nbconvert --ClearOutputPreprocessor.enabled=True --inplace\n",
    "```"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hands-on: implement MCA multiprocessing directly in modelbase\n",
    "\n",
    "- clone the modelbase repository\n",
    "  - `git clone git@gitlab.com:qtb-hhu/modelbase-software.git`\n",
    "  - OR if you don't have ssh set up `git clone https://gitlab.com/qtb-hhu/modelbase-software.git`\n",
    "- Create a new branch for your changes\n",
    "- Open up `modelbase-software/src/modelbase/utils/mca.py`\n",
    "- Identify, which functions need to be changed\n",
    "- Identify, which conditions need to be checked (e.g. operating system)\n",
    "- Implement the functionality\n",
    "- Test locally if it works\n",
    "- Increase the version number\n",
    "- Commit your changes with a semantic commit that follows the [gitchangelog configuration](https://gitlab.com/qtb-hhu/modelbase-software/-/blob/main/.gitchangelog.rc)\n",
    "  - e.g. `usr: new: optional multiprocessing for calculating response coefficients`\n",
    "- Run gitchangelog and commit the updated changelog\n",
    "- Create a merge request for your changes\n",
    "\n",
    "Then in the end we will compare the different solutions and actually include this new functionality in modelbase"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "py311",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
